LaTeX-Kurs
==========

Um die PDFs zu generieren, bitte `lualatex` verwenden:

~~~
lualatex lecture1.tex 
...
~~~

Für Lecture8 sind zuerst die Beispiele im `examples`-Verzeichnis zu erzeugen:

~~~
cd examples
bash gen-theme-examples
bash gen-aspect-examples
pdflatex beamerex1
pdflatex beamerex2
pdflatex beamerex3
pdflatex beamerex4
~~~

