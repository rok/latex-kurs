\documentclass[beamer, %handout,
               compress,
               t,
               xcolor=table,
               aspectratio=141]
               {beamer}

\usepackage{soul}
\usepackage[normalem]{ulem}

\input{common.sty}

\usepackage[T1]{fontenc}
\usepackage{euler}
\usepackage{calligra}

% \usepackage{fontspec,unicode-math}

% \setmathfont[version=lm]{Latin Modern Math}
% \setmathfont[version=palatino]{Asana Math}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
%% Title, author, etc.
%%
\subtitle{Textformatierung}

\begin{document}

% titleframe without head-/foot-line
\begin{frame}[plain]
  \titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Textabstände}

\begin{secframe}

  Leerzeichen und einfache Zeilenumbrüche werden von \latex ignoriert:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.55\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,escapechar=\@]
        Dieser Text@\textvisiblespace\textvisiblespace\textvisiblespace@hat
        @\textvisiblespace\textvisiblespace\textvisiblespace\textvisiblespace@mehr@\textvisiblespace\textvisiblespace@Leerzeichen
        @\textvisiblespace\textvisiblespace@als
        @\textvisiblespace\textvisiblespace\textvisiblespace\textvisiblespace@notwendig.  
      \end{lstlisting}
    \end{column}
    \begin{column}{.40\textwidth}
      \begin{ltxout}
        Dieser Text   hat
            mehr  Leerzeichen
          als
            notwendig.  
      \end{ltxout}
    \end{column}
  \end{columns}

  Erst zwei Zeilenumbrüche führen zu einem neuen Absatz.
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.55\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,escapechar=\@]
        Dieser Text hat keine @ü@berfl@ü@ssigen
        Leerzeichen.

        Und l@ä@sst sich damit besser lesen.
      \end{lstlisting}
    \end{column}
    \begin{column}{.40\textwidth}
      \begin{ltxout}
        Dieser Text hat keine \"uberfl\"ussigen Leerzeichen.

        Und l\"asst sich damit besser lesen.
      \end{ltxout}
    \end{column}
  \end{columns}

  Mehr als zwei Zeilenumbrüche werden aber ebenfalls ignoriert.

\end{secframe}

\subsection{Horizontale Abstände}

\begin{secframe}

  Ein einfaches Leerzeichen erhält man mit \eltxcmd{nobreakspace} oder mit \emph{\textbackslash\textvisiblespace}.

  Ein kurzer Abstand wird mit \eltxcmd{,}\,, größere mit \eltxcmd{quad} oder \eltxcmd{qquad}
  erzeugt.

  Beliebige horizontale Abstände werden mit \eltxcmd[länge]{hspace} definiert.
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.52\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,escapechar=\@,emph={[7]nobreakspace,quad,qquad,hspace}]
        Zwei\nobreakspace\@\textvisiblespace@{}Leerzeichen.

        Ein kurzer\,Abstand.

        Ein langer\quad@\emph{\{\}}@Abstand.

        Ein l@ä@ngerer\qquad@\emph{\{\}}@Abstand.

        Ein beliebiger\hspace{3mm}Abstand.
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        Zwei\nobreakspace\nobreakspace{}Leerzeichen.

        Ein kurzer\,Abstand.

        Ein langer\quad{}Abstand.

        Ein längerer\qquad{}Abstand.

        Ein beliebiger\hspace{3mm}Abstand.
      \end{ltxout}
    \end{column}
  \end{columns}

\end{secframe}

\begin{secframe}

  Ein besonderer horizontaler Abstand ist \eltxcmd{hfill}. Hiermit wird ein Abstand eingefügt, der den
  nachfolgenden Text an das Ende der aktuellen Zeile verschiebt:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.52\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]hfill}]
        Anfang und \hfill Ende.
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        \noindent Anfang und \hfill Ende.
      \end{ltxout}
    \end{column}
  \end{columns}

  Ist der Text länger als eine Zeile, so wird die aktuelle Zeile maximal aufgefüllt:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.52\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]hfill}]
        Text mit \hfill ganz viel ... .
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        \noindent Text mit \hfill ganz viel weiterem Text.
      \end{ltxout}
    \end{column}
  \end{columns}

\end{secframe}

\subsection{Vertikale Abstände}

\begin{secframe}

  Folgende Standardabstände zwischen Absätzen sind definiert:
  \begin{center}
    \vspace{-.5em}
    \begin{tabular}{ll}
      \ltxcmd{smallskip} & ca. 1/4 Zeile \\
      \ltxcmd{medskip} & ca. 1/2 Zeile \\
      \ltxcmd{bigskip} & ca. 1 Zeile \\
    \end{tabular}
  \end{center}

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.52\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]smallskip,medskip,bigskip}]
        Textzeile
        \smallskip

        Textzeile
        \medskip

        Textzeile
        \bigskip

        Textzeile
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        Textzeile
        \smallskip

        Textzeile
        \medskip

        Textzeile
        \bigskip

        Textzeile
      \end{ltxout}
    \end{column}
  \end{columns}
  
\end{secframe}

\begin{secframe}

  Ein beliebiger, vertikaler Abstand läßt sich mittels \eltxcmd[länge]{vspace} erzeugen: 
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.52\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]vspace}]
        Textzeile
        \vspace{2mm}

        Textzeile
        \vspace{5mm}

        Textzeile
        \vspace{1cm}

        Textzeile
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        Textzeile
        \vspace{2mm}

        Textzeile
        \vspace{5mm}

        Textzeile
        \vspace{1cm}

        Textzeile
      \end{ltxout}
    \end{column}
  \end{columns}
  
\end{secframe}

\begin{secframe}

  Analog zu \ltxcmd{hfill} gibt es \eltxcmd{vfill}, welcher den nachfolgenden Text an das Ende der aktuellen
  Seite verschiebt:
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.52\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]vfill}]
        Anfang und
        \vfill
        Ende.
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        \begin{minipage}[t][2.5cm]{1.0\linewidth}
          Anfang und
          \vfill
          Ende.
        \end{minipage}
      \end{ltxout}
    \end{column}
  \end{columns}
  \vspace{.5em}
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.52\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]vfill}]
        Text mit
        \vfill
        viel Rest ... .
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        \begin{minipage}[t][2.5cm]{1.0\linewidth}
          Text mit
          \vfill
          \blindsatz
        \end{minipage}
      \end{ltxout}
    \end{column}
  \end{columns}

\end{secframe}

\subsection{Zeilen und Absätze}

\begin{secframe}

  Ein einfacher Zeilenumbruch wird mit \eltxcmd{\textbackslash} oder \eltxcmd{newline}
  erzeugt.

  Zusätzlich gibt es den Befehl \eltxcmd{linebreak}, mit welchem man die Priorität eines
  Zeilenumbruchs an einer Textstelle mittels optionalem Argument festlegt (0: schwach, 4:
  Zwang).
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.40\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]newline,linebreak}]
        Ein\\ Umbruch\newline zuviel.

        Ein schwacher\linebreak[1]
        Umbruch und ein
        erzwungener\linebreak[4]
        Umbruch.
      \end{lstlisting}
    \end{column}
    \begin{column}{.53\textwidth}
      \begin{ltxout}
        Ein\\ Umbruch\newline zuviel.

        Ein schwacher\linebreak[1] Umbruch und
        ein erzwungener\linebreak[4] Umbruch.
      \end{ltxout}
    \end{column}
  \end{columns}

  Im Gegensatz zu \ltxcmd{newline} wird bei \ltxcmd{linebreak} die Zeile \emph{vor} dem
  Umbruch bis zum Ende \emph{gestreckt}.

  Analog zu \ltxcmd{linebreak} läßt sich mit \eltxcmd{nolinebreak[n]} ein Umbruch an einer
  Stelle (priorisiert) verhindern.
\end{secframe}

\begin{secframe}
  
  Ein neuer Absatz beginnt mit mindestens zwei Zeilenumbrüchen.

  Er kann aber auch mit \eltxcmd{par} erzwungen werden. 
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.52\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]par}]
        Ein\\ Zeilenumbruch.
        
        Ein\par neuer Absatz.
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        Ein\\ Zeilenumbruch.

        \vspace{1em}
        Ein\par neuer Absatz.
      \end{ltxout}
    \end{column}
  \end{columns}

\end{secframe}

\subsection{Abstand und Einrücken}

\begin{secframe}

  Je nach Dokumenttyp wird die erste Absatzzeile eingerückt. Wie weit, definiert die Länge \emph{parindent}.

  Analog wird der Abstand zwischen zwei Absätzen über die Länge \emph{parskip} festgelegt.

  Beides kann mit dem Befehl \eltxcmd{setlength} geändert werden:

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.45\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]parindent,parskip,setlength}]
        Weit hinten, ...\par
        Weit hinten, ...
      \end{lstlisting}
    \end{column}
    \begin{column}{.50\textwidth}
      \begin{ltxoutsm}
        \blindtext\par \blindtext
      \end{ltxoutsm}
    \end{column}
  \end{columns}
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.45\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]parindent,parskip,setlength}]
        \setlength{\parindent}{0cm}
        \setlength{\parskip}{1em}
        Weit hinten, ...\par
        Weit hinten, ...
      \end{lstlisting}
    \end{column}
    \begin{column}{.50\textwidth}
      \begin{ltxoutsm}
        \setlength{\parindent}{0cm}
        \setlength{\parskip}{1em}
        \blindtext\par \blindtext
      \end{ltxoutsm}
    \end{column}
  \end{columns}

  Um ein Zeileneinrücken zu verhindern, wird \eltxcmd{noindent} vor den Absatz gesetzt.

  \vspace{-0.5em}
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.45\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]parindent,parskip,setlength}]
        Weit hinten, ...\par
        \noindent Weit hinten, ...
      \end{lstlisting}
    \end{column}
    \begin{column}{.50\textwidth}
      \begin{ltxoutmid}
        Weit hinten, hinter den Wortbergen, fern \ldots\\[0.5em]
        \noindent Weit hinten, hinter den Wortbergen, fern \ldots
      \end{ltxoutmid}
    \end{column}
  \end{columns}
  
\end{secframe}

\begin{secframe}
  Beim Absatzabstand gibt man \latex üblicherweise ein wenig Flexibilität nach oben und unten, um den Text auf
  einer Seite besser setzen zu können:

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.6\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]plus,minus}]
        \setlength{\parskip}{1em plus 2pt minus 1pt}
        \blindtext\par \blindtext
      \end{lstlisting}
    \end{column}
    \begin{column}{.35\textwidth}
      \begin{ltxoutsm}
        \setlength{\parskip}{1em plus 2pt minus 1pt}
        \blindtext\par \blindtext
      \end{ltxoutsm}
    \end{column}
  \end{columns}

  Mit \emph{plus} wird der Spielraum nach oben und mit \emph{minus} nach unten definiert.
\end{secframe}

\subsection{Längenangaben}

\begin{secframe}
  \latex versteht verschiedene Längenangaben:

  \begin{center}
    \vspace{-1em}
    \begin{tabular}{ll}
      Einheit & \\
      \hline
      \texttt{mm, cm}\hspace{2em} & metrische Angaben \\
      \texttt{in} & Zoll \\
      \texttt{pt} & Punkt (2,85pt $\sim$ 1mm)\\
      \texttt{1ex} & Höhe eines kleinen \emph{x} \\
      \texttt{1em} & Breite eines großen \emph{M} \\
    \end{tabular}
  \end{center}

  Bei den Längenangaben dürfen auch Kommastellen und negative Werte verwendet werden:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.45\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8]
        Text\hspace{2.5pt}Text

        \vspace{-1.25ex}
        Text

        \vspace{0.5em}
        Text\hspace{-1em}Text
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        Text\hspace{2.5pt}Text

        \vspace{-1.25ex}
        Text

        \vspace{0.5em}
        Text\hspace{-1em}Text
      \end{ltxout}
    \end{column}
  \end{columns}
  
\end{secframe}

\subsection{Seiten}

\begin{secframe}

  Normalerweise entscheidet \latex, wann eine neue Seite beginnt.

  Mit \eltxcmd{newpage} kann aber eine neue Seite erzwungen werden.
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.3\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8]
        \blindtext

        \blindtext
      \end{lstlisting}
    \end{column}
    \begin{column}{.3\textwidth}
      \begin{ltxoutsm}
        \blindtext

        \blindtext
      \end{ltxoutsm}
    \end{column}
    \begin{column}{.3\textwidth}
      \begin{ltxoutsm}
        \vspace{1.63cm}
      \end{ltxoutsm}
    \end{column}
  \end{columns}

  \vspace{1em}
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.3\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]newpage}]
        \blindtext

        \newpage
        \blindtext
      \end{lstlisting}
    \end{column}
    \begin{column}{.3\textwidth}
      \begin{ltxoutsm}
        \blindtext
        \vspace{0.75cm}
      \end{ltxoutsm}
    \end{column}
    \begin{column}{.3\textwidth}
      \begin{ltxoutsm}
        \blindtext
        \vspace{0.75cm}
      \end{ltxoutsm}
    \end{column}
  \end{columns}

\end{secframe}

\begin{secframe}
  
  Ähnlich beendet \eltxcmd{clearpage} und \eltxcmd{cleardoublepage} eine Seite und erzwingt die Ausgabe von
  Abbildungen oder Tabellen auf den nächsten Seiten (Gleitobjekte).

  Bei \ltxcmd{cleardoublepage} wird dabei optional eine leere Seite eingefügt, um die nächste Textseite bei
  einer ungeraden Seitennummer zu beginnen (Buch-Dokument).
  
  Analog zu \ltxcmd{linebreak} kann mittels \eltxcmd{pagebreak} ein Seitenumbruch priorisiert bzw. mit
  \eltxcmd{nopagebreak} verhindert werden.

  Zusätzlich gibt es die Umgebung \emph{samepage}, in welche Seitenumbrüche nur zwischen Absätzen erlaubt
  sind.
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.45\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]samepage}]
        \begin{samepage}
          ...
        \end{samepage}
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
    \end{column}
  \end{columns}
  
\end{secframe}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Schriftarten}

\subsection{Schriftfamilien}

\begin{secframe}
  \latex bringt verschiedene Schriftfamilien mit:
  \begin{center}
    \begin{tabular}{llll}
      \texttt{\ltxcmd[Text]{textrm}}     & \ltxcmd{rmfamily} & \textrm{Beispiel} & mit Serifen \\
      \texttt{\ltxcmd[Text]{textsf}}     & \ltxcmd{sffamily} & \textsf{Beispiel} & ohne Serifen \\
      \texttt{\ltxcmd[Text]{texttt}}     & \ltxcmd{ttfamily} & \ttfamily{Beispiel} & Schreibmaschine \\
      \texttt{\ltxcmd[Text]{textnormal}} & \ltxcmd{normalfont} & \textnormal{Beispiel} &  \\
      \hline 
      \texttt{\ltxcmd[Text]{textbf}}     & \ltxcmd{bfseries} & \textbf{Beispiel} & fett \\
      \texttt{\ltxcmd[Text]{textmd}}     & \ltxcmd{mdseries} & \textmd{Beispiel} & normal \\
      \hline 
      \texttt{\ltxcmd[Text]{textit}}     & \ltxcmd{itshape} & \textit{Beispiel} & kursiv \\
      \texttt{\ltxcmd[Text]{textsl}}     & \ltxcmd{slshape} & \textsl{Beispiel} & schräggestellt \\
      \texttt{\ltxcmd[Text]{textsc}}     & \ltxcmd{scshape} & \textsc{Beispiel} & Kapitälchen \\
      \texttt{\ltxcmd[Text]{textup}}     & \ltxcmd{upshape} & \textup{Beispiel} & aufrecht \\
      \hline
      \texttt{\ltxcmd[Text]{emph}}       & & \emph{Beispiel} & hervorgehoben \\
    \end{tabular}
  \end{center}

\end{secframe}

\begin{secframe}
  Neben der Befehlsform, können die Schriftfamilien auch per \latex-Umgebung genutzt werden:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.40\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]rmfamily,ttfamily,bfseries,scshape}]
        \begin{rmfamily}
          ...
        \end{rmfamily}

        \begin{ttfamily}
          ...
        \end{ttfamily}

        \begin{bfseries}
          ...
        \end{bfseries}

        \begin{scshape}
          ...
        \end{scshape}
      \end{lstlisting}
    \end{column}
    \begin{column}{.55\textwidth}
      \begin{ltxoutmid}
        \begin{rmfamily}
          \blindtext
        \end{rmfamily}

        \begin{ttfamily}
          \blindtext
        \end{ttfamily}

        \begin{bfseries}
          \blindtext
        \end{bfseries}

        \begin{scshape}
          \blindtext
        \end{scshape}
      \end{ltxoutmid}
    \end{column}
  \end{columns}
\end{secframe}

\begin{secframe}

  Die Texthervorhebung mittels \ltxcmd{emph} ist abhängig von der Umgebung:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.45\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]emph}]
        Dieser \emph{Text ist 
        \emph{doppelt}} hervorgehoben.
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        \noindent Dieser \emph{Text ist \emph{doppelt}}
        hervorgehoben.
      \end{ltxout}
    \end{column}
  \end{columns}
\end{secframe}

\subsection{Schriftgrößen}

\begin{secframe}

  Die Standardschriftgrößen von \latex sind:
  \begin{center}
    \begin{tabular}{lll}
      \texttt{\ltxcmd{tiny}}         & \tiny Text & extrem klein \\
      \texttt{\ltxcmd{scriptsize}}   & \scriptsize Text & sehr klein \\
      \texttt{\ltxcmd{footnotesize}} & \footnotesize Text & wie Fußnoten \\
      \texttt{\ltxcmd{small}}        & \small Text & klein \\
      \hline 
      \texttt{\ltxcmd{normalsize}}   & \normalsize Text & normal \\
      \hline 
      \texttt{\ltxcmd{large}}        & \large Text & ein wenig größer \\
      \texttt{\ltxcmd{Large}}        & \Large Text & groß \\
      \texttt{\ltxcmd{LARGE}}        & \LARGE Text & noch größer \\
      \hline 
      \texttt{\ltxcmd{huge}}         & \huge Text & ganz groß \\
      \texttt{\ltxcmd{Huge}}         & \Huge Text & riesig \\
    \end{tabular}
  \end{center}
\end{secframe}

\begin{secframe}
  
  Die \latex-Befehle können \small verschieden angewandt werden:

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.48\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,escapechar=\@,emph={[7]tiny,large,huge}]
        Ab \tiny hier wirkt die @Ä@nderung.
        
        Nur {\large in den Klammern} ist es
        anders.

        \begin{huge}
          Oder auch in der ganzen Umgebung
        \end{huge}
      \end{lstlisting}
    \end{column}
    \begin{column}{.47\textwidth}
      \begin{ltxoutmid}
        \setlength{\parskip}{1em}
        \noindent {Ab \Tiny hier wirkt die \"Anderung.}

        \noindent Nur {\large in den Klammern} ist es anders.

        \noindent \begin{huge}Oder auch in der ganzen Umgebung\par\end{huge}
      \end{ltxoutmid}
    \end{column}
  \end{columns}

  Die Empfehlung ist, Größenänderungen \emph{immer} in Klammern oder Umgebungen einzuschließen:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.48\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,escapechar=\@,emph={[7]tiny}]
        \begin{center}
          \tiny
          \blindsatz
        \end{center}
      \end{lstlisting}
    \end{column}
    \begin{column}{.47\textwidth}
      \begin{ltxoutmid}
        \begin{center}
          \Tiny
          \blindsatz
        \end{center}
      \end{ltxoutmid}
    \end{column}
  \end{columns}

\end{secframe}

\begin{secframe}

  Vorsicht bei Änderungen mit Zeilenumbrüchen und Absätzen:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.50\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,escapechar=\@]
        {\large Dieser Text ist etwas
          gr@öß@er.}
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        \noindent{\large Dieser Text ist etwas
          gr\"oßer.}
      \end{ltxout}
    \end{column}
  \end{columns}

  Der Zeilenabstand ist zu klein, da das Absatzende in der normalen Schriftgröße gesetzt
  wird.

  Daher das Ende des Absatzes \emph{innerhalb} der Größenänderung definieren:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.50\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,escapechar=\@,emph={[7]par}]
        {\large Dieser Text ist etwas
          gr@öß@er.\par}
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        \noindent {\large Dieser Text ist etwas
          gr\"oßer.\par}
      \end{ltxout}
    \end{column}
  \end{columns}
\end{secframe}

\subsection{Schriftarten}

\begin{secframe}

  Die Standard-Schriftart von \latex ist \emph{"`Computer Modern"'} (cm).

  Diese Schriftart unterstützt Schrift mit und ohne Serifen, Schreibmaschinenschrift und mathematische
  Formeln.

  Daneben bietet \latex eine Vielzahl weiterer Schriftarten, welche im Normalfall per
  \ltxcmd[schriftart]{usepackage} eingebunden wird.

  Ein Beispiel ist \emph{Latin Modern}, welche eine für die PDF-Ausgabe aktualisierte Version von "`Computer
  Modern"' ist. Der dazugehörige Paketname ist \emph{lmodern}:
  
  \begin{lstlisting}[style=ltxcode,gobble=4,emph={[7]lmodern}]
    \usepackage[T1]{fontenc}
    \usepackage{lmodern}
  \end{lstlisting}

\end{secframe}

\begin{secframe}

  Einige Schriftpakete verändern durch das Einbinden die Standardschriften.

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.50\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]concrete}]
        \usepackage[T1]{fontenc}
        \usepackage{concrete}
        ...

        Weit hinten ...
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        \usefont{T1}{ccr}{m}{n}
        \noindent\blindsatz
      \end{ltxout}
    \end{column}
  \end{columns}

  Bei anderen Paket ist die neue Schrift per Befehl zu aktivieren.

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.50\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]calligra}]
        \usepackage[T1]{fontenc}
        \usepackage{calligra}
        ...

        Weit hinten ...

        {\calligra Weit hinten ...}
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        \noindent\blindsatz
        
        \noindent{\calligra\blindsatz\par}
      \end{ltxout}
    \end{column}
  \end{columns}
  
\end{secframe}

\begin{secframe}

  Einige Schriftpaket wirken sich nur auf bestimmte Schriftarten aus, z.B. in mathematischen Umgebungen:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.50\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]euler}]
        \usepackage[T1]{fontenc}
        \usepackage{euler}
        ...

        Eine Formel:
        \begin{equation*}
          \int_0^{\infty} \frac{\sin x} ...
        \end{equation*}
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        %\mathversion{lm}
        Eine Formel:
        \begin{equation*}
          \int_{0}^{\infty} \frac{\sin x}{\sqrt{x}} \mathrm{d} x = \sqrt{\frac{\pi}{2}}
        \end{equation*}
      \end{ltxout}
      % \begin{ltxout}
      %   %\mathversion{palatino}
      %   \vspace{-1.5em}
      %   \begin{equation*}
      %     \int_{0}^{\infty} \frac{\sin x}{\sqrt{x}} \mathrm{d} x = \sqrt{\frac{\pi}{2}}
      %   \end{equation*}
      % \end{ltxout}
    \end{column}
  \end{columns}
  
\end{secframe}

\begin{secframe}

  Eine Auflistung von verfügbaren \latex-Schriften findet man unter
  \begin{center}
    \emph{http://www.tug.dk/FontCatalogue/}
  \end{center}
  Wichtig ist hierbei die Verfügbarkeit unter \TeX-Live. Ansonsten muss man sich um die Installation selbst
  kümmern.

  Zu jeder Schriftart ist auch der entsprechende Befehl für das Aktivieren (so nötig) angegeben.
\end{secframe}

\subsubsection{Xe-\latex und Lua-\latex}

\begin{secframe}

  Es existieren Varianten von \latex, welche die Verwaltung von Schriften deutlich erweitern und
  vereinfachen.

  Hier ist dann auch die Verwendung von normalen \emph{TrueType-} (ttf) oder \emph{OpenType-} (otf)
  Schriftarten möglich.

  \begin{lstlisting}[style=ltxcode,gobble=4,emph={[6]setmainfont,setmathrm}]
    \documentclass{scrartcl}
    \usepackage{fontspec}
    \usepackage{unicode-math}

    \setmainfont{TeX Gyre Schola}
    \setmathrm{TeX Gyre Schola Math}

    \begin{document}
    ...
    \end{document}
  \end{lstlisting}
\end{secframe}

\subsection{Textmarkierung}

\begin{secframe}

  Neben \ltxcmd{emph} bietet \latex noch \eltxcmd{underline} um Text unterstrichen darzustellen.
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.50\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]underline}]
        ... \underline{Beispiel} ...
      \end{lstlisting}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{ltxout}
        ... \underline{Beispiel} ...
      \end{ltxout}
    \end{column}
  \end{columns}
  
  Im Paket \emph{soul} sind weitere Markierungsbefehle für Text definiert.
  \begin{center}
    \begin{tabular}{lll}
      \textbf{Befehl} & Bedeutung & Aussehen \\
      \hline
      \eltxcmd[Beispiel]{so}   & gesperrt        & \so{Beispiel} \\[0.25em]
      \eltxcmd[Beispiel]{caps} & Kapitälchen     & \caps{Beispiel} \\[0.25em]
      \eltxcmd[Beispiel]{ul}   & unterstrichen   & \ul{Beispiel} \\[0.25em]
      \eltxcmd[Beispiel]{st}   & durchgestrichen & \st{Beispiel} \\
    \end{tabular}
  \end{center}
\end{secframe}

\begin{secframe}
  
  Weitere Befehle für das Unter- und Durchstreichen findet man im \emph{ulem}-Paket. Ausserdem wird
  \ltxcmd{emph} neu definiert.
  \begin{center}
    \begin{tabular}{lll}
      \textbf{Befehl} & Bedeutung & Aussehen \\
      \hline
      \eltxcmd[Beispiel]{uline}     & einfach unterstrichen & \uline{Beispiel} \\[0.25em]
      \eltxcmd[Beispiel]{uuline}    & doppelt unterstrichen & \uuline{Beispiel} \\[0.25em]
      \eltxcmd[Beispiel]{uwave}     & wellig unterstrichen & \uwave{Beispiel} \\[0.25em]
      \eltxcmd[Beispiel]{dashuline} & durchgestrichen & \dashuline{Beispiel} \\[0.25em]
      \eltxcmd[Beispiel]{dotuline}  & durchgestrichen & \dotuline{Beispiel} \\[0.25em]
      \hline
      \eltxcmd[Beispiel]{sout}  & durchgestrichen & \sout{Beispiel} \\[0.25em]
      \eltxcmd[Beispiel]{xout}  & weggestrichen & \xout{Beispiel} \\
      \hline
      \ltxcmd[Beispiel]{emph}  & hervorgehoben & \uline{Beispiel} \\
      \ltxcmd[{\textbackslash emph\{Beispiel\}}]{emph}  & doppelt hervorgehoben & \uuline{Beispiel} \\
    \end{tabular}
  \end{center}

\end{secframe}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Textformatierung}

\subsection{Textausrichtung}

\begin{secframe}

  Standardmäßig wird in \latex Blocksatz verwendet.
  
  Es gibt aber drei Standardumgebungen, um das zu ändern: \emph{flushleft}, \emph{center} und
  \emph{flushright}:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.40\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]flushleft,center,flushright}]
        \begin{flushleft}
          ...
        \end{flushleft}

        
        \begin{center}
          ...
        \end{center}


        \begin{flushright}
          ...
        \end{flushright}
      \end{lstlisting}
    \end{column}
    \begin{column}{.55\textwidth}
      \begin{ltxoutsm}
        \begin{flushleft}
          \blindtext\blindtext
        \end{flushleft}

        \begin{center}
          \blindtext\blindtext
        \end{center}

        \begin{flushright}
          \blindtext\blindtext
        \end{flushright}
      \end{ltxoutsm}
    \end{column}
  \end{columns}

\end{secframe}

\begin{secframe}

  Das selbe Resultat erhält man mit den Befehlen \eltxcmd{raggedright}, \eltxcmd{raggedleft} und
  \eltxcmd{centering}:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.40\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]raggedright,centering,raggedleft}]
        \raggedright
        \blindtext
        \blindtext

        \centering
        \blindtext
        \blindtext

        \raggedleft
        \blindtext
        \blindtext
      \end{lstlisting}
    \end{column}
    \begin{column}{.55\textwidth}
      \begin{ltxoutsm}
        \blindtext\raggedright\blindtext

        \blindtext\centering\blindtext

        \blindtext\raggedleft\blindtext
      \end{ltxoutsm}
    \end{column}
  \end{columns}

  \emph{Aber Vorsicht}: ohne Klammern kann man nicht mehr auf Blocksatz zurückschalten!
\end{secframe}

\subsection{Zitat-Umgebungen}

\begin{secframe}

  Zwei vordefinierte \latex-Umgebung sind für Zitate vorgesehen: \emph{quote} und \emph{quotation}.

  Bei einem einzelnen Satz wird die \texttt{quote}-Umgebung empfohlen:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.30\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]quote}]
        Ein kurzes Zitat:
        
        \begin{quote}
          ...
        \end{quote}
      \end{lstlisting}
    \end{column}
    \begin{column}{.65\textwidth}
      \begin{ltxoutsm}
        Ein kurzes Zitat:
        
        \begin{quote}
          \blindsatz
        \end{quote}
      \end{ltxoutsm}
    \end{column}
  \end{columns}

  Bei einem größeren Textstück, etwa einem gesamten Absatz, sollte \texttt{quotation} genutzt werden:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.30\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]quotation},escapechar=\@]
        Ein l@ä@ngeres Zitat:
        
        \begin{quotation}
          ...
        \end{quotation}
      \end{lstlisting}
    \end{column}
    \begin{column}{.65\textwidth}
      \begin{ltxoutsm}
        Ein längeres Zitat:
        
        \begin{quotation}
          \blindtext

          \blindtext
        \end{quotation}
      \end{ltxoutsm}
    \end{column}
  \end{columns}
\end{secframe}

\subsection{\texttt{minipage}-Umgebung}

\begin{secframe}

  Eine besondere und vielseitig einsetzbare Umgebung ist \emph{minipage}.

  Hier kann man die Breite eines Textes definieren, wobei der gesamte Text weiterhin als Teil eines laufenden
  Absatzes betrachtet wird.

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.35\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]minipage}]
        \begin{minipage}{5cm}
          Weit hinten, ...
        \end{minipage}
      \end{lstlisting}
    \end{column}
    \begin{column}{.60\textwidth}
      \begin{ltxoutmid}
        \begin{minipage}{4cm}
          \blindtext
        \end{minipage}
      \end{ltxoutmid}
    \end{column}
  \end{columns}

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.35\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]minipage}]
        \begin{minipage}{3cm}
          Weit hinten, ...
        \end{minipage}
        \hfill
        \begin{minipage}{3cm}
          Weit hinten, ...
        \end{minipage}
      \end{lstlisting}
    \end{column}
    \begin{column}{.60\textwidth}
      \begin{ltxoutmid}
        \begin{minipage}{2cm}
          \blindtext
        \end{minipage}
        \hfill
        \begin{minipage}{2cm}
          \blindtext
        \end{minipage}
      \end{ltxoutmid}
    \end{column}
  \end{columns}
  
\end{secframe}

\begin{secframe}

  \latex bietet zusätzliche Längen, welche im Zusammenhang mit \texttt{minipage} nützlich sind:
  \begin{center}
    \begin{tabular}{ll}
      \eltxcmd{textwidth} & Breite des Textbereiches einer Seite \\
      \eltxcmd{linewidth} & Breite der aktuellen Zeile \\
      \eltxcmd{columnwidth} & Breite der aktuellen Textspalte \\
    \end{tabular}
  \end{center}

  Diese Längen können mit Faktoren skaliert werden:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.45\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]linewidth}]
        \begin{minipage}{0.6\linewidth}
          Weit hinten, ...
        \end{minipage}
        \begin{minipage}{0.3\linewidth}
          Weit hinten, ...
        \end{minipage}
      \end{lstlisting}
    \end{column}
    \begin{column}{.50\textwidth}
      \begin{ltxoutmid}
        \begin{minipage}[b]{0.6\linewidth}
          \blindsatz
        \end{minipage}
        \begin{minipage}{0.3\linewidth}
          \blindsatz
        \end{minipage}
      \end{ltxoutmid}
    \end{column}
  \end{columns}
\end{secframe}

\begin{secframe}

  \texttt{minipage} hat noch optionale Argumente, mit denen
  \begin{itemize}
  \item die Ausrichtung der Miniseite an der aktuellen Zeile (\emph{t,c,b}),
  \item die Höhe der Miniseite und
  \item die Ausrichtung des Textinhaltes bzgl. der Seitenhöhe (\emph{t,c,b})
  \end{itemize}
  angegeben werden kann:
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.45\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]c,t,b}]
        \begin{minipage}[b][2cm][t]{...}
          Weit hinten, ...
        \end{minipage}
        \begin{minipage}[c][2cm][c]{...}
          Weit hinten, ...
        \end{minipage}
        \begin{minipage}[t][2cm][b]{...}
          Weit hinten, ...
        \end{minipage}
      \end{lstlisting}
    \end{column}
    \begin{column}{.50\textwidth}
      \begin{ltxoutmid}
        \noindent
        \fbox{\begin{minipage}[b][2cm][t]{0.25\linewidth}
          \blindsatz
        \end{minipage}}
        \fbox{\begin{minipage}[c][2cm][c]{0.25\linewidth}
          Weit hinten, ...
        \end{minipage}}
        \fbox{\begin{minipage}[t][2cm][b]{0.25\linewidth}
          \blindsatz
        \end{minipage}}
      \end{ltxoutmid}
    \end{column}
  \end{columns}
\end{secframe}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% TeX-engine: luatex
%%% ispell-local-dictionary: "german"
%%% End:
