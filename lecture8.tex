\documentclass[beamer, %handout,
               compress,
               t,
               xcolor=table,
               aspectratio=141]
               {beamer}

\input{common.sty}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
%% Darstellung einer PDF-Seite
%%
\newsavebox{\lecexampbox}
\newlength{\lecexampboxh}
\newlength{\lecexampboxd}
\newlength{\lecexampboxw}
\NewDocumentCommand{\lecexamp}{ m m }{%
  \pgfdeclareimage[page=#2,width=\textwidth]{lexexampimg}{examples/#1}%
  \savebox{\lecexampbox}{\pgfuseimage{lexexampimg}}%
  \settoheight{\lecexampboxh}{\usebox{\lecexampbox}}%
  \settodepth{\lecexampboxd}{\usebox{\lecexampbox}}%
  \settowidth{\lecexampboxw}{\usebox{\lecexampbox}}%
  \begin{tikzpicture}%
    \fill[schlagschatten] (0,0) rectangle ++(\lecexampboxw,\lecexampboxh);%
    \draw node [anchor=south west,inner sep=0mm] at (0,0) {\pgfuseimage{lexexampimg}};%
  \end{tikzpicture}}

%%
%% Themen-Beispiel
%%
\NewDocumentCommand{\beamerextheme}{ m }{%
  \begin{block}{#1}%
    \begin{columns}[T,onlytextwidth]%
      \begin{column}{.49\textwidth}%
        \lecexamp{beamer-theme-#1.pdf}{1}%
      \end{column}%
      \begin{column}{.49\textwidth}%
        \lecexamp{beamer-theme-#1.pdf}{2}%
      \end{column}%
    \end{columns}%
  \end{block}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subtitle{Präsentationen}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Einführung}

\subsection{Folien}

\begin{secframe}

  Das am meisten genutzte Paket zur Erstellung von Präsentationen mit \latex ist \emph{beamer}.

  Hierin implementiert ist die Dokumentenklasse \emph{beamer} und die Umgebung \emph{frame}, welche zusammen
  einzelne Folien erzeugen.
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]beamer,frame},escapechar=\@]
        \documentclass{beamer}
        
        \begin{document}
          \begin{frame}{Folientitel}
          ...
          \end{frame}
        \end{document}
      \end{lstlisting}

      \vspace{.5em}
      Alternativ:
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]frametitle},escapechar=\@]
          \begin{frame}
            \frametitle{Folientitel}
          ...
          \end{frame}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \lecexamp{beamerex1}{2}
    \end{column}
  \end{columns}

  Folien können neben dem Titel (mit \eltxcmd{frametitle}) auch Untertitel besitzen. Hierzu wird der Befehl
  \eltxcmd{framesubtitle} genutzt.
\end{secframe}

\subsection{Titelseite}

\begin{secframe}

  Analog zu anderen Dokumenten werden die Daten der Titelseite definiert, d.h. mit \ltxcmd{title},
  \ltxcmd{author}, \ltxcmd{date}, usw.. Hinzukommen \eltxcmd{subtitle} und \eltxcmd{institute}.

  Angezeigt wird die Titelseite mit dem Befehl \eltxcmd{titlepage}.
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]subtitle,institute,titlepage},escapechar=\@]
        \title{\LaTeX für Nichtwissenschaftler}
        \subtitle{Präsentationen}
        \author{Ronald Kriemann}
        \institute{MPI MIS}
        \date{\today}
      
        \begin{frame}
          \titlepage
        \end{frame}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \lecexamp{beamerex1}{1}
    \end{column}
  \end{columns}
\end{secframe}

\begin{secframe}

  Für die Daten der Titelseite gibt es optionale Kurzformen, welche dann auf den einzelnen Folien angezeigt
  werden.

  Angegeben wird die Kurzform als optionales Argument.
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.41\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]},escapechar=\@]
        \title@\Emph{[\textbackslash LaTeX-Kurs]}@{\LaTeX für ...}
        \subtitle{Präsentationen}
        \author@\Emph{[R.Kriemann]}@{Ronald Kriemann}
        \institute@\Emph{[MPI]}@{MPI MIS}
        \date{\today}
      \end{lstlisting}
    \end{column}
    \begin{column}{.58\textwidth}
      \lecexamp{beamerex3}{6}
    \end{column}
  \end{columns}
\end{secframe}

\subsection{Übersicht}

\begin{secframe}

  Eine Übersicht über den Vortrag wird, wie üblich, mit \ltxcmd{tableofcontents} erzeugt.

  Auch hier kann die Hierarchietiefe mit dem Zähler \texttt{tocdepth} eingestellt werden.
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]tableofcontents,tocdepth},escapechar=\@]
        \begin{frame}{"Ubersicht}
          \setcounter{tocdepth}{2}
          \tableofcontents
        \end{frame}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \lecexamp{beamerex3}{2}
    \end{column}
  \end{columns}
\end{secframe}

\begin{secframe}

  Das Inhaltsverzeichnis in Beamer kann mit Optionen modifizert werden.
  
  \begin{block}{\texttt{pausesections}} \adjustparskip
    Hiermit wird das Inhaltsverzeichnis Schritt für Schritt aufgebaut.
    \begin{columns}[T,onlytextwidth]
      \begin{column}{.49\textwidth}
        \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]pausesections},escapechar=\@]
          \begin{frame}{"Ubersicht}
            \tableofcontents[pausesections]
          \end{frame}
        \end{lstlisting}
      \end{column}
      \begin{column}{.49\textwidth}
        \only<1>{\lecexamp{beamerex3}{3}}
        \only<2->{\lecexamp{beamerex3}{4}}
      \end{column}
    \end{columns}
    Noch feiner kann die Übersicht mit \texttt{pausesubsections} aufgebaut werden.
  \end{block}
\end{secframe}

\begin{secframe}
  \begin{block}{\texttt{currentsection}} \adjustparskip
    Mit \texttt{currentsection} wird der aktuelle Abschnitt hervorgehoben.
    \begin{columns}[T,onlytextwidth]
      \begin{column}{.49\textwidth}
        \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]currentsection},escapechar=\@]
          \begin{frame}{"Ubersicht}
            \tableofcontents[currentsection]
          \end{frame}
        \end{lstlisting}
      \end{column}
      \begin{column}{.49\textwidth}
        \lecexamp{beamerex3}{5}
      \end{column}
    \end{columns}
    Analog kann man mit \texttt{currentsubsection} das jeweilige Unterkapitel hervorheben.
  \end{block}
\end{secframe}

\subsection{Themen}

\begin{secframe}

  Das Aussehen von Folien wird mittels Beamer-Themen festgelegt. Hierzu dient der Befehl
  \eltxcmd[thema]{usetheme}. Er darf nur im Dokumentenkopf verwendet werden.

  Verschiedene Themen werden bei Beamer mitgeliefert. Einige dieser Themen definieren auch zusätzliche
  Navigationsfunktionen oder Randleisten.

  \pause
  \only<2>{\beamerextheme{default}}%
  \only<3>{\beamerextheme{Bergen}}%
  \only<4>{\beamerextheme{Madrid}}%
  \only<5>{\beamerextheme{AnnArbor}}%
  \only<6>{\beamerextheme{CambridgeUS}}%
  \only<7>{\beamerextheme{EastLansing}}%
  \only<8>{\beamerextheme{Antibes}}%
  \only<9>{\beamerextheme{Montpellier}}%
  \only<10>{\beamerextheme{Berkeley}}%
  \only<11>{\beamerextheme{Goettingen}}%
  \only<12>{\beamerextheme{Marburg}}%
  \only<13>{\beamerextheme{Hannover}}%
  \only<14>{\beamerextheme{Berlin}}%
  \only<15>{\beamerextheme{Dresden}}%
  \only<16>{\beamerextheme{Szeged}}%
  \only<17>{\beamerextheme{Luebeck}}%
  \only<18>{\beamerextheme{Warsaw}}
\end{secframe}

\begin{secframe}

  Möchte man komplett auf ein Thema verzichten, d.h. keine Kopf- und Fußzeilen anzeigen,
  so erreicht man das durch die Option \emph{plain} der \texttt{frame}-Umgebung.

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]plain},escapechar=\@]
        \begin{frame}[plain]
          \vbox to \textheight{
            \vfill
            \begin{center}
              \usebeamercolor[fg]{frametitle}%
              \usebeamerfont{frametitle}%
              \scalebox{1.5}{\insertsection}
            \end{center}
            \vfill
          }
        \end{frame}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \lecexamp{beamerex3}{8}
    \end{column}
  \end{columns}
  
\end{secframe}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Befehle und Umgebungen}

\subsection{Block-Umgebungen}

\begin{secframe}

  Für einzelne Textblöcke bringt Beamer die \emph{block}-Umgebung mit, welche einen abgesetzten Abschnitt auf
  der Folie darstellt. Als Argument wird der Name des Blockes mitgegeben.

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]block},escapechar=\@]
        ...
        \begin{block}{Erstens}
          ...
        \end{block}
        ...
        \begin{block}{Zweitens}
          ...
        \end{block}
        ...
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \lecexamp{beamerex2}{2}
    \end{column}
  \end{columns}
  
\end{secframe}

\begin{secframe}

  Neben \texttt{block} gibt es noch Block-Umgebungen für Beispiele (\emph{exampleblock}) und wichtige
  Informationen (\emph{alertblock}).
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]exampleblock,alertblock},escapechar=\@]
        \begin{exampleblock}{Beispiel}
          ...
        \end{exampleblock}

        \begin{alertblock}{Wichtig}
          ...
        \end{alertblock}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \lecexamp{beamerex2}{38}
    \end{column}
  \end{columns}
  
\end{secframe}

\subsection{Listen}

\begin{secframe}

  Beamer verändert die Ausgabe von Listen (\texttt{itemize}, \texttt{enumerate} und \texttt{description}).

  Bei \texttt{enumerate} kann analog zum \texttt{enumerate}-Paket die Nummerierung (\texttt{i,I,a,A})
  verändert werden.
  
  Zusätzlich kann bei \texttt{description} die Breite der Beschriftung per optionalem Argument mitgegeben
  werden.

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]block},escapechar=\@]
        \begin{itemize}
        ...
        \end{itemize}

        \begin{enumerate}@\Emph{[i]}@
        ...
        \end{enumerate}

        \begin{description}@\Emph{[Drei]}@
          \item[Eins] ...
          \item[Zwei] ...
          \item[Drei] ...
        \end{description}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \lecexamp{beamerex2}{3}
    \end{column}
  \end{columns}
  
\end{secframe}

\subsection{Mehrere Spalten}

\begin{secframe}

  Für die Darstellung in mehreren Spalten bietet Beamer die \emph{columns}-Umgebung. Innerhalb dieser wird mit
  der \emph{column}-Umgebung jeweils eine neue Spalte definiert, wobei die Spaltenbreite als Parameter
  festgelegt wird.
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]columns,column},escapechar=\@]
        \begin{columns}
          \begin{column}{.2\textwidth}
            ...
          \end{column}
          \begin{column}{.5\textwidth}
            ...
          \end{column}
          \begin{column}{.3\textwidth}
            ...
          \end{column}
        \end{columns}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \lecexamp{beamerex2}{4}
    \end{column}
  \end{columns}
  
\end{secframe}

\begin{secframe}

  Standardmäßig werden der Spalteninhalt zentriert angeordnet. Dies kann mittels optionalem Parameter für
  \texttt{columns} geändert werden (\emph{t}op, \emph{c}enter, \emph{b}ottom).

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]},escapechar=\@]
        \begin{columns}@\Emph{[t]}@
          ...
        \end{columns}
      \end{lstlisting}
      \lecexamp{beamerex2}{5}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]},escapechar=\@]
        \begin{columns}@\Emph{[b]}@
          ...
        \end{columns}
      \end{lstlisting}
      \lecexamp{beamerex2}{6}
    \end{column}
  \end{columns}
  
\end{secframe}

\begin{secframe}

  Der Bereich für die Anzeige der Spalten ist größer, als der normale Textbereich. Um dies zu reduzieren gibt
  es die Option \texttt{onlytextwidth}.
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]},escapechar=\@]
        \begin{columns}
          ...
        \end{columns}
      \end{lstlisting}
      \lecexamp{beamerex2}{7}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]},escapechar=\@]
        \begin{columns}@\Emph{[onlytextwidth]}@
          ...
        \end{columns}
      \end{lstlisting}
      \lecexamp{beamerex2}{8}
    \end{column}
  \end{columns}
\end{secframe}

\subsection{\ltxcmd{alert}}

\begin{secframe}

  Beamer definiert zusätzlich zu \ltxcmd{emph} die Alternative \eltxcmd{alert}. Hierbei wird der Text
  farbig eingefärbt.
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]alert},escapechar=\@]
        ... \emph{...} ...

        ... \alert{...} ...
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \lecexamp{beamerex2}{9}
    \end{column}
  \end{columns}
\end{secframe}

\subsection{Navigations-Symbole}

\begin{secframe}

  Ob die Navigationssymbole eingeblendet werden oder nicht, läßt sich über
  \begin{lstlisting}[style=ltxcode,gobble=4,emph={[7]setbeamertemplate},escapechar=\@]
    \setbeamertemplate{@\Emph{navigation symbols}@}...
  \end{lstlisting}
  \vspace{-0.5em}
  steuern. Hierbei muss der Befehl \emph{vor} einer \texttt{frame}-Umgebung stehen.

  \vspace{1em}
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      Vordefinierte Optionen sind
      {\setbeamertemplate{description item}[align left]
        \begin{description}[\texttt{[horizontal]}]
        \item[{\texttt{[default]}}] Standarddarstellung des Themas
        \item[\alert<2>{\texttt{[horizontal]}}] horizontale Darstellung
        \item[\alert<3>{\texttt{[vertical]}}] vertikale Darstellung
        \end{description}}
    \end{column}
    \begin{column}{.49\textwidth}
      \only<1-2>{\lecexamp{beamerex2}{36}}%
      \only<3->{\lecexamp{beamerex2}{37}}
    \end{column}
  \end{columns}
  

  Um die Anzeige zu deaktivieren verwendet man
  \begin{lstlisting}[style=ltxcode,gobble=4,emph={[7]alert},escapechar=\@]
    \setbeamertemplate{navigation symbols}@\Emph{\{\}}@
  \end{lstlisting}
\end{secframe}

\subsection{Notizen}

\begin{secframe}
  Beamer unterstützt auch Notizen innerhalb von Folien, welche bei der eigentlichen
  Darstellung normalerweise nicht ausgegeben werden.

  Hierfür wird der Befehl \eltxcmd[Text]{note} genutzt.

  Sollen die Notizen angezeigt werden, so kann dies mit
  \eltxcmd[show~notes]{setbeameroption} erreicht werden. Die Ausgabe erfolgt dann auf
  einer Extraseite.
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]setbeameroption,note},escapechar=\@]
        \setbeameroption{show notes}

        \begin{frame}{Notizen}
          ...

          \note{Eine Notiz für mich.}
          \note{Kurz fassen!}
        \end{frame}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \lecexamp{beamerex3}{10}
    \end{column}
  \end{columns}
\end{secframe}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Overlays}

\begin{secframe}

  Beamer ist in der Lage, Inhalte einer einzelnen Folie nacheinander auszugeben. Dies wird als \emph{Overlay}
  bezeichnet.

  In der einfachsten Version, bei der der Folieninhalt sequentiell dargestellt wird, kommt der Befehl
  \eltxcmd{pause} zum Einsatz. Bei jedem Pausen-Befehl ist hierbei ein Weiterblättern nötig.
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]pause},escapechar=\@]
        ...
        \pause
        ...
        \begin{itemize}
        \item Stichpunkt 1
          \pause
        \item Stichpunkt 2
          \pause
        \item Stichpunkt 3
        \end{itemize}
        ...
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{ltxout}
        \blindsatz
        \pause

        \begin{itemize}
        \item Stichpunkt 1
          \pause
        \item Stichpunkt 2
          \pause
        \item Stichpunkt 3
        \end{itemize}
        \onslide<1->
      \end{ltxout}
    \end{column}
  \end{columns}

\end{secframe}

\begin{secframe}
  Allgemein kommt die Overlayspezifikation \emph{<n-m>} zum Einsatz, welche bei \latex-Befehlen
  angehängt wird. Hierbei steht \emph{n} für die erste Unterfolie und \emph{m} für die letzte Unterfolie, auf
  welcher der Inhalt dargestellt werden soll.

  Der Start oder das Ende können auch weggelassen werden (\emph{<-m>} bzw. \emph{<n->}), oder es kann nur eine
  einzelne bzw. mehrere Unterfolien angegeben werden (\emph{<n>}, \emph{<$n_1,n_2,n_3$>}).

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]},escapechar=\@]
        Eine \textcolor@\Emph{<1>}@{red}{Liste} mit
        \textcolor@\Emph{<4>}@{blue}{Overlays}:
        \begin{itemize}
        \item@\Emph{<1-2>}@ Stichpunkt 1
        \item@\Emph{<2->}@ Stichpunkt 2
        \item@\Emph{<3>}@ Stichpunkt 3
        \end{itemize}

        \pause
        Weit hinten, ...
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{ltxout}
        Eine \textcolor<1>{red}{Liste} mit \textcolor<4>{blue}{Overlays}:
        \begin{itemize}
        \item<1-2> Stichpunkt 1
        \item<2-> Stichpunkt 2
        \item<3> Stichpunkt 3
        \end{itemize}

        \pause
        \blindsatz
        \onslide<1->
      \end{ltxout}
    \end{column}
  \end{columns}

\end{secframe}

\begin{secframe}

  Bei Listen kann man auch eine Standarddefinition für Overlays mitgeben:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]},escapechar=\@]
        \begin{itemize}@\Emph{[<+->]}@
        \item Stichpunkt 1
        \item Stichpunkt 2
        \item Stichpunkt 3
        \end{itemize}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{ltxout}
        \begin{itemize}[<+->]
        \item Stichpunkt 1
        \item Stichpunkt 2
        \item Stichpunkt 3
        \end{itemize}
      \end{ltxout}
    \end{column}
  \end{columns}

  Hierbei steht das "`\emph{\texttt{+}}"' für eine automatische, fortlaufende Nummerierung der Overlays.

  Soll ein Pausen-Befehl bei einer bestimmten Unterfolie aktiv werden, so kann man deren Nummer als optionalen
  Parameter mitgeben.
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]},escapechar=\@]
        \begin{itemize}[<+->]
        \item Stichpunkt 1
        \item Stichpunkt 2
        \item Stichpunkt 3
        \end{itemize}
        \pause@\Emph{[2]}@
        ...
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{ltxout}
        \begin{itemize}[<+->]
        \item Stichpunkt 1
        \item Stichpunkt 2
        \item Stichpunkt 3
        \end{itemize}

        \pause[5]
        \blindsatz
        \onslide<1->
      \end{ltxout}
    \end{column}
  \end{columns}
  
\end{secframe}

\subsection{Overlay-Befehle}

\begin{secframe}

  Beamer bietet verschiedene, explizite Befehle, um Overlay-Spezifikationen anzuwenden:
  \begin{description}[{\oltxcmd[sicht.][sonst]{alt}}]
  \item[{\oltxcmd[]{onslide}}] Inhalt nur auf den angegebenen Unterfolien anzeigen. Sonst nur Platz beanspruchen.
  \item[{\oltxcmd[]{only}}] Inhalt nur auf angegebenen Unterfolien anzeigen. Sonst \emph{kein} Platz beanspruchen.
  \item[{\oltxcmd[]{uncover}}] Inhalt nur auf angegebenen Unterfolien angezeigen. Sonst nicht oder transparent
    angezeigen. Platz wird immer beansprucht. 
  \item[{\oltxcmd[]{visible}}] Wie \ltxcmd{uncover} aber Inhalt wird nie transparent dargestellt.
  \item[{\oltxcmd[]{invisible}}] Gegenteil von \ltxcmd{visible}.
  \item[{\oltxcmd[sicht.][sonst]{alt}}] Auf angegebenen Folien "`\texttt{sicht.}"' anzeigen. Ansonsten "`\texttt{sonst}"'.
  \end{description}
\end{secframe}

\begin{secframe}
  
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]onslide,only,uncover,visible,invisible,alt},escapechar=\@]
        \begin{itemize}
          \onslide<2->{\item Stichpunkt Eins}
          \only<3>{\item Stichpunkt Zwei}
          \uncover<4-5>{\item Stichpunkt Drei}
          \visible<5>{\item Stichpunkt Vier}
          \invisible<5>{\item Stichpunkt Fünf}
          \item \alt<1-4>{Stichpunkt Sechs}{Ende}
        \end{itemize}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{ltxout}
        \begin{itemize}
          \onslide<2->{\item Stichpunkt Eins}
          \only<3>{\item Stichpunkt Zwei}
          \uncover<4-5>{\item Stichpunkt Drei}
          \visible<5>{\item Stichpunkt Vier}
          \invisible<5>{\item Stichpunkt Fünf}
        \item \alt<1-4>{Stichpunkt Sechs}{Ende}
        \end{itemize}
      \end{ltxout}
    \end{column}
  \end{columns}

  \vspace{1em}
  \pause[7]
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]onslide,only,uncover,visible,invisible,alt},escapechar=\@]
        \pgfdeclareimage{minerva}{minerva}
        \pgfdeclareimage{logo}{MIS-Logo}
        \only<1,3>{\pgfuseimage{minerva}}%
        \only<2,4>{\pgfuseimage{logo}}
        
        \onslide<1,2>{\pgfuseimage{minerva}}%
        \onslide<3,4>{\pgfuseimage{logo}}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{ltxout}
        \pgfdeclareimage[width=2cm]{minerva}{minerva}
        \pgfdeclareimage[width=2cm]{logo}{pics/MIS-Logo}
        \noindent\only<7,9>{\pgfuseimage{minerva}}%
        \only<8,10>{\pgfuseimage{logo}}
        
        \noindent\onslide<7,8>{\pgfuseimage{minerva}}%
        \onslide<9,10>{\pgfuseimage{logo}}
        \onslide<1->
      \end{ltxout}
    \end{column}
  \end{columns}
\end{secframe}

\subsection{Transparenz}

\begin{secframe}

  Nicht angezeigte Unterfolien können in \latex mit verschiedenen Stufen der Transparenz
  angezeigt werden.

  Eingestellt wird dies mit \eltxcmd[mode]{setbeamercovered}, wobei "`\emph{mode}"' die
  folgenden Werte haben kann:
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{description}[\texttt{highly dynamic}]
      \item[\alert<2>{\texttt{invisible}}] nicht anzeigen,
      \item[\alert<3>{\texttt{transparent}}] transparent anzeigen (\texttt{transparent=grad}).
      \item[\alert<4>{\texttt{dynamic}}] je weiter weg desto transparenter.
      \item[\alert<5>{\texttt{highly dynamic}}] Wie \texttt{dynamic}, aber stärker.
      \end{description}
    \end{column}
    \begin{column}{.49\textwidth}
      \only<1>{\lecexamp{beamerex2}{10}}%
      \only<2>{\lecexamp{beamerex2}{11}}%
      \only<3>{\lecexamp{beamerex2}{16}}%
      \only<4>{\lecexamp{beamerex2}{21}}%
      \only<5>{\lecexamp{beamerex2}{26}}%
    \end{column}
  \end{columns}
\end{secframe}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Paket-Optionen}

\subsection{\texttt{handout} und \texttt{draft}}

\begin{secframe}

  Wird beim Laden von Beamer die Option \emph{handout} angegeben, so werden alle Overlays
  einer Folie zu einer zusammengefaßt. Dies ist für einen Ausdruck der Folien hilfreich.
  \begin{lstlisting}[style=ltxcode,gobble=4,emph={[7]handout},escapechar=\@]
    \documentclass[handout]{beamer}
  \end{lstlisting}
  
  Mit \emph{draft} wird anstelle der normalen Kopf- und Fußzeilen nur ein grauer Balken
  angezeigt.

  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      \begin{lstlisting}[style=ltxcode,gobble=8,emph={[7]draft},escapechar=\@]
        \documentclass[draft]{beamer}
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      \only<1>{\lecexamp{beamerex3}{6}}%
      \only<2->{\lecexamp{beamerex4}{1}}
    \end{column}
  \end{columns}
\end{secframe}

\subsection{\texttt{aspectratio}}

\begin{secframe}

  Als Standard haben die Folien in Beamer ein Seitenverhältnis von 4:3, was älteren
  Projektoren entspricht (Auflösung von $1024 \times 768$).

  Neuere Projektoren haben dagegen ein Seitenverhältnis von 16:10 oder 16:9 (FullHD).

  Mit der Option \emph{aspectratio=format} läßt sich das Format anpassen. Durch die
  verschiedenen Folienabmessungen kann aber auch unterschiedlich viel Text dargestellt
  werden.

  \vspace{.5em}
  \begin{columns}[T,onlytextwidth]
    \begin{column}{.49\textwidth}
      Mögliche Werte für "`format"' sind
      {\setbeamertemplate{description item}[align left]
      \begin{description}[\textbf{1610xxx}]
      \item[\alert<1>{\texttt{43}}] 4:3
      \item[\alert<2>{\texttt{169}}] 16:9
      \item[\alert<3>{\texttt{1610}}] 16:10
      \item[\alert<4>{\texttt{149}}] 14:9
      \item[\alert<5>{\texttt{141}}] 1.41:1
      \item[\alert<6>{\texttt{54}}] 5:4
      \item[\alert<7>{\texttt{32}}] 3:2
      \end{description}}
    \end{column}
    \begin{column}{.49\textwidth}
      \vspace{-1em}
      \only<1>{\lecexamp{beamer-aspect-43}{2}}%
      \only<2>{\lecexamp{beamer-aspect-169}{2}}%
      \only<3>{\lecexamp{beamer-aspect-1610}{2}}%
      \only<4>{\lecexamp{beamer-aspect-149}{2}}%
      \only<5>{\lecexamp{beamer-aspect-141}{2}}%
      \only<6>{\lecexamp{beamer-aspect-54}{2}}%
      \only<7>{\lecexamp{beamer-aspect-32}{2}}%
    \end{column}
  \end{columns}
\end{secframe}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% TeX-engine: luatex
%%% ispell-local-dictionary: "german"
%%% End:
